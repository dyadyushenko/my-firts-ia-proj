let result = '\n';
for (let row = 1; row <= 8; row += 1) {
  for (let cell = 1; cell <= 8; cell += 1) {
    result += (row % 2 === cell % 2) ? '#"' : ' ';
  }
  result += '\n';
}
console.log(result);
